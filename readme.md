# Project template

## 开发配置

```bash
# 安装依赖
npm i

# 开始开发
npm run dev

# 打包
npm run build

# 查看打包日志
npm run build-p

```
