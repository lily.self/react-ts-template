const path = require('path');
const webpack = require('webpack');
const progress = require('cli-progress');
const merge = require('webpack-merge').default;
const TerserPlugin = require('terser-webpack-plugin');
const CSSMinimizerPlugin = require('css-minimizer-webpack-plugin');
const CSSExtractPlugin = require('mini-css-extract-plugin');
const getBaseConfig = require('./webpack.config.base');
const utils = require('./utils');

function getProductionConfig(dir) {
  const bar = new progress.SingleBar({
    format: '{bar} {percentage}% {info}',
    barsize: 20,
  }, progress.Presets.shades_classic);
  let isHasStart = false;
  const plugin = new webpack.ProgressPlugin((percent, message, ...args) => {
    if (percent < 1 && !isHasStart) {
      bar.start(1, 0);
      isHasStart = true;
    }
    bar.update(Math.round(percent * 100) / 100, {
      info: [message, ...args].join(' '),
    });
    if (percent === 1) {
      bar.stop();
      console.clear();
    }
  });

  return merge(getBaseConfig(dir), {
    module: {
      rules: utils.styleLoader('production'),
    },
    output: {
      path: path.resolve(dir, 'dist'),
      clean: true,
      publicPath: './',
    },
    mode: 'production',
    optimization: {
      // 如果只压缩js，可以不设置该字段
      minimizer: [
        new TerserPlugin(),
        new CSSMinimizerPlugin(),
        new CSSExtractPlugin(),
      ],
      splitChunks: {
        chunks: 'all',
      },
    },
    plugins: [plugin],
  });
}

module.exports = getProductionConfig;
