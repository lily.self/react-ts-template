const path = require('path');
const WebpackEslintPlugin = require('eslint-webpack-plugin');
const WebpackHtmlPlugin = require('html-webpack-plugin');

function getBaseConfig(dir) {
  return {
    entry: path.resolve(dir, 'src/index.ts'),
    output: {
      filename: '[name].[contenthash].js',
    },
    resolve: {
      extensions: ['.ts', '.tsx', '.js', 'jsx'],
      alias: {
        '@lib': path.resolve(dir, 'lib'),
        '@': path.resolve(dir, 'src'),
      },
    },
    module: {
      rules: [
        {
          test: /\.(png|jpe?g|gif|svg)$/,
          type: 'asset',
        },
        {
          test: /\.(jsx?)$/,
          use: ['babel-loader', {
            loader: 'ts-loader',
            options: {
              transpileOnly: process.env.NODE_ENV === 'development',
            },
          }],
          exclude: /node_modules/,
        },
        {
          test: /\.(tsx?|module\.js)$/,
          use: ['babel-loader', {
            loader: 'ts-loader',
            options: {
              transpileOnly: process.env.NODE_ENV === 'development',
            },
          }],
        },
      ],
    },
    plugins: [
      new WebpackEslintPlugin({
        extensions: ['js', 'ts', 'jsx', 'tsx'],
      }),
      new WebpackHtmlPlugin({
        template: path.resolve(dir, 'public/index.html'),
      }),
    ],
  };
}

module.exports = getBaseConfig;
