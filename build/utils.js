const path = require('path');
const os = require('os');
const CSSExtractPlugin = require('mini-css-extract-plugin');

function resolve(dir) {
  return path.resolve(__dirname, '..', dir);
}

function getIp() {
  const faces = os.networkInterfaces();
  const keys = Object.keys(faces);
  for (let i = 0; i < keys.length; i += 1) {
    const arr = faces[keys[i]];
    for (let j = 0; j < arr.length; j += 1) {
      if (arr[j].family === 'IPv4' && arr[j].address !== '127.0.0.1') {
        return arr[j].address;
      }
    }
  }

  return '127.0.0.1';
}

function getLoaders(mode, isNodeModules, styleName) {
  const isDevelopment = mode === 'development';
  const cssLoader = {
    loader: 'css-loader',
    options: {
      sourceMap: isDevelopment,
      modules: {
        localIdentName: '[local]_[hash:base64:10]',
      },
    },
  };
  const postcssLoader = {
    loader: 'postcss-loader',
    options: {
      sourceMap: isDevelopment,
    },
  };
  let loaders = [cssLoader, postcssLoader];
  if (isNodeModules) {
    loaders = ['css-loader'];
  }
  if (styleName && styleName !== 'css') {
    loaders.unshift(`${styleName}-loader`);
  }
  const prefixLoader = isDevelopment ? 'style-loader' : CSSExtractPlugin.loader;
  loaders.unshift(prefixLoader);
  return loaders;
}

function styleLoader(mode) {
  const styleArr = ['css', 'less', 'scss', 'stylus'];
  const result = [];
  for (let i = 0; i < styleArr.length; i += 1) {
    const regExp = new RegExp(`\\.${styleArr[i]}$`);
    result.push({
      test: regExp,
      use: getLoaders(mode, false, styleArr[i]),
      exclude: /node_modules/,
    });
    result.push({
      test: regExp,
      use: getLoaders(mode, true, styleArr[i]),
      include: /node_modules/,
    });
  }
  return result;
}

module.exports = {
  resolve,
  getIp,
  styleLoader,
};
