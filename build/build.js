const chalk = require('chalk');
const webpack = require('webpack');
const utils = require('./utils');
const getProductionConfig = require('./webpack.config.production');

function build(dir) {
  const config = getProductionConfig(dir);

  webpack(config, (err, stats) => {
    if (err) {
      console.log(chalk.red(err.message));
      return;
    }
    console.log(stats.toString({
      colors: true,
      modules: false,
      chunks: false,
      chunkModules: false,
      children: false,
    }));

    if (stats.hasErrors()) {
      console.log(chalk.red('> Build failed with errors'));
      process.exit(1);
    }

    console.log(chalk.bold.green('> Build complete'));
  });
}

build(utils.resolve(''));
