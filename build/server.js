const webpack = require('webpack');
const chalk = require('chalk');
const path = require('path');
const WebpackServer = require('webpack-dev-server');
const portfinder = require('portfinder');
const utils = require('./utils');
const getDevelopmentConfig = require('./webpack.config.development');

async function start(dir) {
  portfinder.basePort = 8080;
  const port = await portfinder.getPortPromise();
  const ip = utils.getIp();
  const config = getDevelopmentConfig(dir, `http://${ip}:${port}`);
  const compiler = webpack(config, (err) => {
    if (err) {
      console.log(chalk.red(err.message));
    }
  });
  const server = new WebpackServer({
    port,
    client: {
      overlay: { errors: true, warnings: false },
      progress: true,
      logging: 'none',
    },
    hot: true,
    static: {
      directory: path.resolve(dir, 'public'),
    },
  }, compiler);

  server.startCallback((startErr) => {
    if (startErr) {
      console.error(startErr);
    }
  });
}

start(utils.resolve(''));
