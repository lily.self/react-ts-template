const chalk = require('chalk');
const webpack = require('webpack');
const progress = require('cli-progress');
const merge = require('webpack-merge').default;
const getBaseConfig = require('./webpack.config.base');
const utils = require('./utils');

function getDevelopmentConfig(dir, uri) {
  const bar = new progress.SingleBar({
    format: '{bar} {percentage}% {info}',
    barsize: 20,
  }, progress.Presets.shades_classic);
  let isHasStart = false;
  const plugin = new webpack.ProgressPlugin((percent, message, ...args) => {
    if (percent < 1 && !isHasStart) {
      bar.start(1, 0);
      isHasStart = true;
    }
    bar.update(Math.round(percent * 100) / 100, {
      info: [message, ...args].join(' '),
    });
    if (percent === 1) {
      bar.stop();
      console.clear();
      console.log(
        chalk.green('Compile success !\n'),
      );
      console.log(
        chalk.bold('> Your application running on:'),
        chalk.bold.green(uri),
      );
    }
  });

  return merge(getBaseConfig(dir), {
    module: {
      rules: utils.styleLoader('development'),
    },
    watch: true,
    mode: 'development',
    stats: 'errors-warnings',
    infrastructureLogging: {
      level: 'warn',
    },
    devtool: 'eval-cheap-module-source-map',
    plugins: [
      plugin,
    ],
  });
}

module.exports = getDevelopmentConfig;
