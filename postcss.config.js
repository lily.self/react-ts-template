/* eslint-disable import/no-extraneous-dependencies */

// https://github.com/michael-ciniawsky/postcss-load-config
const autoprefixer = require('autoprefixer');

const plugins = [autoprefixer()];

module.exports = {
  plugins,
};
