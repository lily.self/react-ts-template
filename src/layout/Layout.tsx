import { Layout, ConfigProvider } from 'antd';
import React from 'react';

const RootLayout: React.FC = (props) => (
  <ConfigProvider>
    <Layout>
      <Layout.Header>
        header
      </Layout.Header>
      <Layout.Content>
        context
        {props.children}
      </Layout.Content>
      <Layout.Footer>
        footer
      </Layout.Footer>
    </Layout>
  </ConfigProvider>
);

export default RootLayout;
