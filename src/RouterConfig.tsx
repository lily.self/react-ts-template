import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import Layout from '@/layout/Layout';

const IndexPage = React.lazy(() => import('./routes/IndexPage'));

export default () => (
  <React.Suspense fallback={'loading'}>
    <Layout>
      <BrowserRouter>
        <Routes>
          <Route path='/' element={<IndexPage />} />
        </Routes>
      </BrowserRouter>
    </Layout>
  </React.Suspense>
);
