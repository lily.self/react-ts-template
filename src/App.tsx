import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'react-redux';
import RouterConfig from './RouterConfig';
import store from './store/store';

function App() {
  return (
    <Provider store={store}>
      <RouterConfig />
    </Provider>
  );
}

function render(root: HTMLElement) {
  ReactDOM.render(<App />, root);
}

export default render;
