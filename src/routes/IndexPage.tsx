import React from 'react';
import { Button } from 'antd';
import styles from './indexPage.css';
import { useRootDispatch, useRootSelector } from '@/hooks';
import { save } from '@/store/slices/count';

function IndexPage() {
  const count = useRootSelector((state) => state.count.count);
  const dispatch = useRootDispatch();

  function add() {
    dispatch(save(count + 1));
  }

  function subtract() {
    dispatch(save(count - 1));
  }

  return (
    <div className={styles.indexPage}>
      <div>count: {count}</div>
      <div>
        <Button onClick={add}>+</Button>
        <Button onClick={subtract}>-</Button>
      </div>
    </div>
  );
}

export default IndexPage;
