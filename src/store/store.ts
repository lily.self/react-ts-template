import { configureStore } from '@reduxjs/toolkit';
import countSlice from './slices/count';

const store = configureStore({
  reducer: {
    [countSlice.name]: countSlice.reducer,
  },
  devTools: process.env.NODE_ENV === 'development',
});

export type RootState = ReturnType<typeof store.getState>;
export type RootDispatch = typeof store.dispatch;

export default store;
