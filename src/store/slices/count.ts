import { createSlice, PayloadAction } from '@reduxjs/toolkit';

const slice = createSlice({
  name: 'count',
  initialState: {
    count: 0,
  },
  reducers: {
    save(state, action: PayloadAction<number>) {
      return { ...state, count: action.payload };
    },
  },
});

export const { save } = slice.actions;

export default slice;
