import { useDispatch, useSelector, TypedUseSelectorHook } from 'react-redux';
import type { RootState, RootDispatch } from '@/store/store';

export const useRootDispatch: () => RootDispatch = useDispatch;

export const useRootSelector: TypedUseSelectorHook<RootState> = useSelector;
